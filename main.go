package main

import ( "github.com/gin-gonic/gin"
				 "github.com/gin-contrib/cors"
				 "strconv" )
func ToInt(str string) int64 {
	i, _ := strconv.ParseInt(str, 10, 64)
	return i
}
func ToString(num int64) string {
	return strconv.FormatInt(num, 10)
}
func Pnum(check int64) bool {
	p := true
	for i := int64(2); i <= (check / 2); i++ {
		if check % i == 0 {
				p = false
				break
		}
	}
	return p
}
func Plist(start int, end int) []int {
	list := []int{}
	for i := start;i <= end;i++ {
		if Pnum(int64(i)) {
			list = append(list, i)
		}
	}
	return list
}
func main() {
	config := cors.DefaultConfig()
	config.AllowAllOrigins = true
	config.AllowCredentials = true
	config.AddAllowHeaders("authorization")
	r := gin.Default()
	r.Use(cors.Default())
	r.Use(cors.New(config))
	r.GET("/", func(c *gin.Context) {
			if c.Query("start") != "" && c.Query("end") != "" {
				c.JSON(200, gin.H{
					"numbers": Plist(int(ToInt(c.Query("start"))),int(ToInt(c.Query("end")))),
				})
			} else {
				c.JSON(400, gin.H{
					"error": "<start> or <end> not defined.",
				})
			}
	})
	r.Run(":1382") // listen and serve on 0.0.0.0:8080
}